import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit {

  dificultad: string

  palabra: string;
  

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) {
    this.route.queryParams.subscribe(params => {
      if(params && params.dificultad){
        this.dificultad = params.dificultad
      }
    })
    
   }

   
   ngOnInit() {
    this.palabra = this.dataService.palabra()
  }

  actionMethod($event: MouseEvent){
    ($event.target as HTMLButtonElement).disabled=true;
  }

  recorrerPalabra(){
    for(let i = 0; i < this.palabra.length; i++){
      console.log(this.palabra.replace(/./gi, "_"))
    }
  }


  goToNextPage(){

    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    }

    this.router.navigate(["result"], navigationExtras)
  }


}