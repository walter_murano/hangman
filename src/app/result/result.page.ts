import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  dificultad: String;


  constructor(private route: ActivatedRoute, private router: Router) {
    this.route.queryParams.subscribe(params => {
      if(params && params.dificultad){
        this.dificultad = params.dificultad
      }
    })
   }

  ngOnInit() {
  }

  goToPreviousPage(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    }

    this.router.navigate(["game"], navigationExtras)
  }

  goToMenu(){
    this.router.navigate(["menu"])
  }

}
