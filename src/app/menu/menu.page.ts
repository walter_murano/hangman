import { Component, OnInit } from '@angular/core';
import { NavigationExtras ,Router } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  dificultad: string;

  constructor(private router: Router) { }

  goToNextPage(){

    let navigationExtras: NavigationExtras = {
      queryParams: {
        dificultad: this.dificultad
      }
    }

    this.router.navigate(["game"], navigationExtras)
  }

  ngOnInit() {
  }

}
